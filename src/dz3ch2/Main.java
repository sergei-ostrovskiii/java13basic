package dz3ch2;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        //добавить книгу в библиотеку
        library.addBook(new Book("Оно", "Стивен Кинг"));
        library.addBook(new Book("Метель", "Пушкин А.С."));
        library.addBook(new Book("Мизери", "Стивен Кинг"));
        library.addBook(new Book("Кладбище домашних животных", "Стивен Кинг"));
        library.addBook(new Book("Тёмная башня", "Стивен Кинг"));
        library.addBook(new Book("Кэрри", "Стивен Кинг"));
        //удалить книгу из библиотеки
        library.deleteBook(new Book("Тёмная башня", "Стивен Кинг"));
        //найти и вернуть книгу по названию
        System.out.println((library.returnBookTitle("Кэрри")).getAuthor() + " " + (library.returnBookTitle("Кэрри")).getTitle());
        //найти и вернуть список книг по автору
        ArrayList<Book> count = new ArrayList<>();
        count = library.returnListBook("Стивен Кинг");
        //проверка списка
        for (int i = 0; i < count.size(); i++) {
            System.out.println("Книга - " + count.get(i).getTitle());
            System.out.println("Автор - " + count.get(i).getAuthor());
        }
    }
}

