package dz3ch2;

import java.util.ArrayList;

public class Library {
    ArrayList<Book> library = new ArrayList<>();

    public void addBook(Book book) {
        if (library.size() > 0) {
            for (int i = 0; i < library.size(); i++) {
                if (library.get(i).getTitle().equals(book.getTitle()) &&
                        library.get(i).getAuthor().equals(book.getAuthor())) {
                    break;
                } else {
                    library.add(book);
                    break;
                }
            }
        } else if (library.size() == 0) {
            library.add(book);
        }
    }

    public void deleteBook(Book book) {
        if (library.size() > 0) {
            for (int i = 0; i < library.size(); i++) {
                if (library.get(i).getTitle().equals(book.getTitle()) && library.get(i).getAuthor().equals(book.getAuthor())) {
                    library.remove(i);
                    break;
                }
            }
        }
    }

    public Book returnBookTitle(String s) {
        Book cont = null;
        if (library.size() > 0) {
            for (Book book : library)
                if (book.getTitle().equals(s)) {
                    cont = book;
                }
        }
        return cont;
    }

    public ArrayList returnListBook(String s) {
        ArrayList<Book> list = new ArrayList<>();
        if (library.size() > 0) {
            for (Book book : library)
                if (book.getAuthor().equals(s)) {
                    list.add(book);
                }
        }
        return list;
    }
}

