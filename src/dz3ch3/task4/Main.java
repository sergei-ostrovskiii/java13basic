package dz3ch3.task4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество участников:");
        int x = sc.nextInt();

        ArrayList<String> name = new ArrayList<>();
        System.out.println("Введите имена хозяев:");
        for (int i = 0; i < x; i++) {
            name.add(sc.next());
        }

        ArrayList<Dog> dogs = new ArrayList<>();
        System.out.println("Введите клички животных");
        for (int i = 0; i < x; i++) {
            dogs.add(new Dog(sc.next()));
        }
        ArrayList<Double> ocenki = new ArrayList<>();
        System.out.println("Введите оценки:");
        for (int i = 0; i < x; i++) {
            ocenki.add(((int) ((((double) sc.nextInt() + (double) sc.nextInt()
                    + (double) sc.nextInt()) / 3) * 10)) / 10.0);
        }

        ArrayList<Participant> winners = new ArrayList<>();
        for (int i = 0; i <x ; i++) {
            winners.add(new Participant(name.get(i), dogs.get(i).getName(), ocenki.get(i)));
        }
        winners.sort(Comparator.comparingDouble(Participant::getOcenki).reversed());
        for (int i = 0; i < 3; i++) { // 3 - количество призовых мест
            System.out.println(winners.get(i).getName() + ": " + winners.get(i).getNickname() + ", "
                    + winners.get(i).getOcenki());

        }
        }
}
