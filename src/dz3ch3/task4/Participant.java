package dz3ch3.task4;

public class Participant {
    private final String name;
    private final String nickname;
    private final double ocenki;

    public Participant(String name, String nickname, double ocenki) {
        this.name = name;
        this.nickname = nickname;
        this.ocenki = ocenki;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public double getOcenki() {
        return ocenki;
    }
}
