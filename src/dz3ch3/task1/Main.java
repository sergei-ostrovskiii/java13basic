package dz3ch3.task1;

public class Main {
    public static void main(String[] args) {
        Dolphin delphin = new Dolphin();
        delphin.birth();
        delphin.sleep();
        delphin.eats();
        delphin.swimming();

        System.out.println();

        GoldFish goldFish = new GoldFish();
        goldFish.birth();
        goldFish.sleep();
        goldFish.eats();
        goldFish.swimming();

        System.out.println();

        Bat bat = new Bat();
        bat.birth();
        bat.sleep();
        bat.eats();
        bat.flying();

        System.out.println();

        Eagle eagle=new Eagle();
        eagle.birth();
        eagle.sleep();
        eagle.eats();
        eagle.flying();
    }
}
