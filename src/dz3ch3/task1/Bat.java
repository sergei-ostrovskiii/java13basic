package dz3ch3.task1;

//летучая мышь
public class Bat extends Mammal implements Flying {
    public Bat() {
        System.out.println("Летучая мышь");
    }

    public void flying() {
        System.out.println("Летает медленно");
    }

}
