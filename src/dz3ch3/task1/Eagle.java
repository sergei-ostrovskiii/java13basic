package dz3ch3.task1;

//Орел
public class Eagle extends Bird implements Flying {
    public Eagle() {
        System.out.println("Орел");
    }

    public void flying() {
        System.out.println("Летает быстро");
    }
}
