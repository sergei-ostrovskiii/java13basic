package dz3ch3.task1;

//Золотая рыбка
public class GoldFish extends Fish implements Swimming {
    public GoldFish() {
        System.out.println("Золотая рыбка");
    }

    public void swimming() {
        System.out.println("Плавает медленно");
    }
}
