package dz3ch3.task1;

//Дельфин
public class Dolphin extends Mammal implements Swimming {

    public Dolphin() {
        System.out.println("Дельфин");
    }

    @Override
    public void swimming() {
        System.out.println("Плавает быстро");
    }
}
