package dz3ch3.task1;

public abstract class Animals {
    /*
    Реализовать что все животные спят, едят и рождаются(eats, sleep end birth)
    но едят и спят одинаково, а рождаются по разному!
     */
   public final void eats(){
       System.out.println("ест");
   }
   public final void sleep(){
       System.out.println("спит");
   }
   public void birth(){
   }

}
