package dz3ch1.task1;

import java.util.concurrent.Callable;

public class Cat {

    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        int a = (int) (Math.random() * 3);
        if (a == 0) {
            sleep();
        } else if (a == 1) {
            meow();
        } else
            eat();
    }
}
