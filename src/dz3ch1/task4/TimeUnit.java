package dz3ch1.task4;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
/*Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
Java 12 Базовый модуль Неделя 6
ДЗ 3 Часть 1
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).
 */
public class TimeUnit {
    private int hour;//час
    private int minutes;//минуты
    private int seconds;//секунды

    public TimeUnit(int h, int m, int s) {
        if (h > 0 && h < 24) {
            hour = h;
        } else {
            System.out.println("Час введен некоректно!");
        }
        if (m > 0 && m < 60) {
            minutes = m;
        } else {
            System.out.println("Минуты введены некоректно!");
        }
        if (s > 0 && s < 60) {
            seconds = s;
        } else {
            System.out.println("Секунды введены некоректно!");
        }
    }

    public TimeUnit(int h, int m) {
        if (h > 0 && h < 24) {
            hour = h;
        } else {
            System.out.println("Час введен некоректно!");
        }
        if (m > 0 && m < 60) {
            minutes = m;
        } else {
            System.out.println("Минуты введены некоректно!");
        }
        seconds = 00;
    }

    public TimeUnit(int h) {
        if (h > 0 && h < 24) {
            hour = h;
        } else {
            System.out.println("Час введен некоректно!");
        }
        minutes = 00;
        seconds = 00;
    }

    public void time24() {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, seconds);
        SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss");
        System.out.println(formater.format(calendar.getTime()));
    }

    public void time12() {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, seconds);
        SimpleDateFormat formater = new SimpleDateFormat("hh:mm:ss");
        System.out.println(formater.format(calendar.getTime()));
    }

    public void timePlus(int h, int m, int s) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, hour + h);
        calendar.set(Calendar.MINUTE, minutes + m);
        calendar.set(Calendar.SECOND, seconds + s);
        SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss");
        System.out.println(formater.format(calendar.getTime()));
    }
}
