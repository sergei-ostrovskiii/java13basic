package dz3ch1.task8;

public class Atm {
    public int courseDollars;
    public int courseRubles;
    private static int counter;
    public Atm(int dollars, int rubles){
        this.courseDollars=dollars;
        this.courseRubles=rubles;
    }
    public void dollarsInRubles(int dollars){
        int rubles=0;
            rubles=dollars*courseRubles;
            counter++;
    }
    public void rublesInDollars(int rubles){
        int dollars=0;
        dollars=rubles/courseDollars;
        counter++;
    }
    public static int count(){
        return counter;
    }
}
