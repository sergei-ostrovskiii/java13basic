package dz3ch1.task6;

import java.util.Arrays;

public class AmazingString {
    private char[] amazing;
    private char[] arr;

    public AmazingString() {

    }

    public AmazingString(String string) {//принемает строку
        amazing = new char[string.length()];
        for (int i = 0; i < string.length(); i++) {
            this.amazing[i] = string.charAt(i);
        }
    }

//    public AmazingString(char[] arr) {//принемиает массив
//        this.arr = arr;
//    }

    public char symbol() {//возвращает 1й символ
        char a = amazing[0];
        return a;
    }

    public int lengthString() {//длина строки
        int s = 0;
        for (int i = 0; i < amazing.length; i++) {
            s++;
        }
        return s;
    }

    public void sout() {
        for (int i = 0; i < amazing.length; i++) {
            System.out.print(amazing[i]);
        }
    }

    public boolean isThereString(char[] arr) {// Проверить, есть ли переданная подстрока в AmazingString (на вход
        boolean x = false;                    // подается массив char). Вернуть true, если найдена и false иначе
        for (int i = 0; i < arr.length; i++) {
            if (amazing[i] == arr[i]) {
                x = true;
            } else {
                x = false;
                break;
            }
        }
        return x;
    }

    public boolean isThereString(String string) {//Проверить, есть ли переданная подстрока в AmazingString (на вход
        boolean x = false;                      // подается String). Вернуть true, если найдена и false иначе
        for (int i = 0; i < string.length(); i++) {
            if (amazing[i] == string.charAt(i)) {
                x = true;
            } else {
                x = false;
                break;
            }
        }
        return x;
    }

    public void spacesDelete() {//Удалить из строки AmazingString ведущие пробельные символы, если они есть
        int x = 0;
        int f = 0;
        char[] arrCopy = new char[amazing.length];
        for (int i = 0; i < amazing.length; i++) {
            f++;
            if (amazing[i] != ' ') {
                x = i;
                break;
            }
        }
        System.out.println(x);
        System.arraycopy(amazing, x, arrCopy, 0, arrCopy.length - (f - 1));
        System.arraycopy(arrCopy, 0, amazing, 0, arrCopy.length);
        System.out.println("Удалить пробелы - " + Arrays.toString(amazing));//саут для проверки=)
    }

    public void expandTheLine() {//Развернуть строку (первый символ должен стать последним, а
        char[] x = new char[amazing.length];
        for (int i = 0; i < amazing.length; i++) {
            x[amazing.length - 1 - i] = amazing[i];
        }
        for (int i = 0; i < amazing.length; i++) {
            amazing[i] = x[i];
        }
        System.out.println("Развернуть строку - " + Arrays.toString(amazing));//соут для проверки
    }
}

