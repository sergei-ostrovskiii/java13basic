package dz3ch1.task6;

public class Main {
    public static void main(String[] args) {
        char[] x = {'s', 'r', 'w', 'w', 'e', 'l'};
        char[] f = {'п', 'р', 'и', 'в', 'е', 'т'};
        AmazingString a = new AmazingString("привет");
        System.out.println("Выводит первый символ - " + a.symbol());
        System.out.println("Выводит длинну сироки - " + a.lengthString());
        a.sout();
        System.out.println();
        System.out.println("Проверяет, есть ли переданная подстрока в AmazingString (на вход\n" +
                "подается массив char)  " + a.isThereString(f));
        System.out.println("Проверяется, есть ли переданная подстрока в AmazingString (на вход\n" +
                "подается String) - " + a.isThereString("привет"));
        a.spacesDelete();
        a.expandTheLine();
    }
}
