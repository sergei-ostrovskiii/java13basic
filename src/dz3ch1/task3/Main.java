package dz3ch1.task3;

import dz3ch1.task2.Student;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student person = new Student();
        Student person1 = new Student();
        Student person2 = new Student();
        Student[] students = {person, person1, person2};
        person2.setName("Сергей");
        person2.setSurname("Островский");
        person2.setGrages(new int[]{5, 3, 9, 9, 8, 3, 6, 7, 8, 9});

        person1.setName("Андрей");
        person1.setSurname("Арсеньев");
        person1.setGrages(new int[]{5, 3, 4, 6, 8, 3, 6, 7, 8, 9});

        person.setName("Петр");
        person.setSurname("Яблоков");
        person.setGrages(new int[]{4, 5, 1, 2, 7, 7, 7, 5, 4, 3});

        System.out.println(StudentService.bestStudent(students).getName() + " "
                + StudentService.bestStudent(students).getSurname());// студент с лучшим средним баллом=)
        System.out.println(students[1].getSurname());
        StudentService.sortBySurname(students);
        System.out.println(Arrays.toString(students));
        System.out.println(students[0].getName() + " " + students[0].getSurname());
        System.out.println(students[1].getName() + " " + students[1].getSurname());
        System.out.println(students[2].getName() + " " + students[2].getSurname());
    }
}
