package dz3ch1.task5;

public class DayOfWeek {
    public byte number;
    public String day;

    public void setNumber(byte n) {
        this.number = n;
    }

    public byte getNumber() {
        return number;
    }

    public void setDay(String d) {
        this.day = d;
    }

    public String getDay() {
        return day;
    }
}
