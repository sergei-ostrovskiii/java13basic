package dz3ch1.task5;

public class Main {
    public static void main(String[] args) {

        DayOfWeek week1 = new DayOfWeek();
        DayOfWeek week2 = new DayOfWeek();
        DayOfWeek week3 = new DayOfWeek();
        DayOfWeek week4 = new DayOfWeek();
        DayOfWeek week5 = new DayOfWeek();
        DayOfWeek week6 = new DayOfWeek();
        DayOfWeek week7 = new DayOfWeek();
        week1.setNumber((byte)1);
        week1.setDay("Monday");
        week2.setNumber((byte)2);
        week2.setDay("Tuesday");
        week3.setNumber((byte)3);
        week3.setDay("Wednesday");
        week4.setNumber((byte)4);
        week4.setDay("Thursday");
        week5.setNumber((byte)5);
        week5.setDay("Friday");
        week6.setNumber((byte)6);
        week6.setDay("Saturday");
        week7.setNumber((byte)7);
        week7.setDay("Sunday");
        DayOfWeek[] dayOfWeeks = {week1, week2, week3, week4, week5, week6, week7};

        for (int i = 0; i < dayOfWeeks.length; i++) {
            System.out.println(dayOfWeeks[i].getNumber() + " " + dayOfWeeks[i].getDay());
        }
    }
}
