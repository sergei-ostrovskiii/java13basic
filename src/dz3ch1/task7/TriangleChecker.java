package dz3ch1.task7;
/*
Реализовать класс TriangleChecker, статический метод которого принимает три
длины сторон треугольника и возвращает true, если возможно составить из них
треугольник, иначе false. Входные длины сторон треугольника — числа типа
double. Придумать и написать в методе main несколько тестов для проверки
работоспособности класса (минимум один тест на результат true и один на
результат false)
 */
public class TriangleChecker {
    public static boolean triangle(double a,double b,double c){
        boolean x=false;
        if(a+b>c){
            if(b+c>a){
                if(a+c>b){
                    x=true;
                }
            }
        }else{
            x=false;
        }
        return x;
    }

}
