package dz2ch2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//столбцы и строки
        int[][] arr = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[j][i] = sc.nextInt();
            }
        }
        boolean sem = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] != arr[(n - 1) - j][(n - 1) - i]) {
                    sem = false;
                }
            }
        }
        System.out.println(sem);
    }
}
