package dz2ch2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//столбцы
        int m = sc.nextInt();//строки
        int[][] arr = new int[n][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr[j][i] = sc.nextInt();
            }
        }
        int x = 1001;
        for (int j = 0; j < m; j++) {
            for (int k = 0; k < n; k++) {
                if (arr[k][j] < x) {
                    x = arr[k][j];
                }
            }
                System.out.print(x + " ");
                x=1001;
        }
    }
}
