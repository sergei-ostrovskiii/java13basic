package dz2ch2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//столбцы и строки
        int[][] arr = new int[n][n];
        int[][] arr2 = new int[n - 1][n - 1];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[j][i] = sc.nextInt();
            }
        }
        int p = sc.nextInt();//число Р
        int w = 0;
        int q = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (p == arr[i][j]) {
                    w = i;
                    q = j;
                }
            }
        }
        int k = -1, l = -1;
        for (int i = 0; i < n; i++) {
            if (i == w) {
                continue;
            }
            k++;
            for (int j = 0; j < n; j++) {
                if (j == q) {
                    continue;
                }
                l++;
                arr2[k][l] = arr[j][i];
            }
            l = -1;
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (j < n - 2) {
                    System.out.print(arr2[i][j] + " ");
                } else {
                    System.out.print(arr2[i][j]);
                }
            }
            System.out.println();
        }
    }
}


