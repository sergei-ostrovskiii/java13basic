package dz2ch2;

import java.util.Scanner;

/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
       int a = 0;
        for (int i = n; i > 0; i = i / 10) {
            a += n % 10;
            n = n / 10;
        }
        System.out.print(a);
    }

}
