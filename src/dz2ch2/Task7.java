package dz2ch2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int uchastniki = sc.nextInt();
        String[][] arr = new String[uchastniki][2];
        double[][] ocenki = new double[uchastniki][3];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < uchastniki; j++) {
                arr[j][i] = sc.next();
            }
        }
        for (int i = 0; i < uchastniki; i++) {
            for (int j = 0; j < 3; j++) {
                ocenki[i][j] = sc.nextInt();
            }
        }
        double[] sum = new double[uchastniki];
        for (int i = 0; i < uchastniki; i++) {
            for (int j = 0; j < 3; j++) {
                sum[i] += ocenki[i][j];
            }
        }
        int d = 0, k = 0, l = 0;
        for (int i = 0; i < uchastniki - 1; i++) {
            if (sum[i] > sum[i + 1]) {
                d = i;
            } else {
                d = uchastniki - 1;
            }
            for (int j = 0; j < uchastniki; j++) {

                if (sum[j] < sum[d] && sum[j] > sum[j + 1]) {
                    k = j;
                }
                if (sum[j] < sum[k] && sum[j] > sum[j + 1]) {
                    l = j;
                }
            }
        }
        System.out.println(arr[d][0] + ": " + arr[d][1] + ", " + (int) ((sum[d] / 3) * 10) / 10.0);
        System.out.println(arr[k][0] + ": " + arr[k][1] + ", " + (int) ((sum[k] / 3) * 10) / 10.0);
        System.out.print(arr[l][0] + ": " + arr[l][1] + ", " + (int) ((sum[l] / 3) * 10) / 10.0);
    }
}
