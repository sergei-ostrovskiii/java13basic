package dz2ch2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a = 0;
        for (int i = n; i > 0; i = i / 10) {
            a= n % 10;
            n = n / 10;
            System.out.print(a+" ");
        }
    }
}
