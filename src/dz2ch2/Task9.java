package dz2ch2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String n = sc.next();

        for (int i = 0; i < n.length(); i++) {
            System.out.print(n.charAt(i) + " ");
        }
    }
}
