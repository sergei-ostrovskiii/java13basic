package dz2ch2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//столбцы и строки
        int[][] arr = new int[n][n];
        int x = sc.nextInt();//номер столбца для изменения
        int y = sc.nextInt();//номер строки для изменения
        int x2 = sc.nextInt();//номер столбца для изменения
        int y2 = sc.nextInt();//номер строки для изменения

        for (int i = x; i <= x2; i++) {//строки
            for (int j = y; j <= y2; j++) {//столбцы
                arr[i][j] = 1;
            }
        }
        for (int i = x + 1; i <= x2 - 1; i++) {
            for (int j = y + 1; j <= y2 - 1; j++) {
                arr[i][j] = 0;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j < n - 1) {
                    System.out.print(arr[j][i] + " ");
                } else {
                    System.out.print(arr[j][i]);
                }
            }
            System.out.println();

        }
    }
}

