package dz2ch2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        int k = sc.nextInt();
        int[][] arr = new int[7][4];

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                arr[i][j] = sc.nextInt();
            }
        }
        int x = 0;
        boolean s = true;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 7; j++) {
                x += arr[j][i];
            }
            if (x > a && i == 0) {
                s = false;
            }
            if (x > b && i == 1) {
                s = false;
            }
            if (x > c && i == 2) {
                s = false;
            }
            if (x > k && i == 3) {
                s = false;
            }
            x = 0;
        }
        if (s) {
            System.out.print("Отлично");
        } else {
            System.out.println("Нужно есть поменьше");
        }
    }
}
