package profile.dz2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main<T> {
    /*
    Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.
     */
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(10);
        arrayList.add(10);
        arrayList.add(11);
        arrayList.add(11);
        arrayList.add(12);
        arrayList.add(12);
        arrayList.add(13);
        arrayList.add(14);

        System.out.println(uniqueElements(arrayList));

    }

    public static <T> Set<T> uniqueElements(ArrayList<T> arr) {
        Set<T> set = new HashSet<>();
        set.addAll(arr);

        return set;

    }
}
