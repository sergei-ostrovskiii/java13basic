package profile.dz2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {
    /*
    . В некоторой организации хранятся документы (см. класс Document). Сейчас все
    документы лежат в ArrayList, из-за чего поиск по id документа выполняется
    неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
    перевести хранение документов из ArrayList в HashMap.
     */
    public static void main(String[] args) {
        ArrayList<Document> arrayList = new ArrayList<>();

        organizeDocuments(arrayList);
        for (Map.Entry<Integer, Document> entry : organizeDocuments(arrayList).entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        for (Document document : documents) {
            map.put(document.id, document);
        }
        return map;

    }

    class Document {
        public int id;
        public String name;
        public int pageCount;
    }
}
