package profile.dz3.task3;

import com.sun.jdi.NativeMethodException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    /*
    Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
     */
    public static void main(String[] args) {
        Class<APrinter> cls = APrinter.class;
        try {
            Method methods = cls.getDeclaredMethod("print", int.class);
            methods.invoke(new APrinter(), 23.0);
        } catch (NoSuchMethodException | InvocationTargetException |
                IllegalAccessException | IllegalArgumentException e) {
            System.out.println(e);
        }
    }
}

