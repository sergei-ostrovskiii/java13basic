package profile.dz3.task2;

import profile.dz3.task1.Islike;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Islike(x = true)
public class Main {
    /*
     Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на
экран.
     */
    public static void main(String[] args) {
        printAnat(Main.class);
    }

    public static void printAnat(Class<?> cls) {
        if (!cls.isAnnotationPresent(Islike.class)) {
            return;
        }
        Islike islike = cls.getAnnotation(Islike.class);
        System.out.println(islike.x());
    }
}
