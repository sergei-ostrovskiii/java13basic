create table buyer
(
    id        serial primary key,
    names     varchar(30) not null,
    telephone varchar(11) not null
);
insert into buyer (names, telephone)
values ('Олег', '89503456234');

insert into buyer (names, telephone)
values ('Анна', '89645538967');

insert into buyer (names, telephone)
values ('Сергей', '89995648297');

select *
from buyer;

