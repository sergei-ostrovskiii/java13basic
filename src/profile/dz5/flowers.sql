create table flowers
(
    id    serial primary key,
    title varchar(30) not null,
    price integer     not null
);

select *
from flowers;

insert into flowers (title, price)
values ('Rose', '100');

insert into flowers (title, price)
values ('Lily', '50');

insert into flowers (title, price)
values ('daisies', '25');