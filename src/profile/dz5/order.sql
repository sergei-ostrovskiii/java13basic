create table orders
(
    id         serial primary key,
    name_id    integer references buyer (id),
    title_id   integer references flowers (id),
    quantity   integer check (quantity > 0 and quantity <= 1000),
    date_added timestamp not null
);

select *
from orders;

insert into orders (name_id, title_id, quantity, date_added)
values ('3', '2', '100', now());
insert into orders (name_id, title_id, quantity, date_added)
values ('2', '1', '10', now());
insert into orders (name_id, title_id, quantity, date_added)
values ('1', '3', '20', now());
insert into orders (name_id, title_id, quantity, date_added)
values ('2', '1', '10', now());
insert into orders (name_id, title_id, quantity, date_added)
values ('2', '3', '200', now());

--По идентификатору заказа получить данные заказа и данные клиента,
--создавшего этот заказ
SELECT*
FROM orders
         JOIN buyer
              ON orders.name_id = buyer.id
         join flowers
              on orders.title_id = flowers.id
where orders.id = 1;

--Получить данные всех заказов одного клиента по идентификатору
--клиента за последний месяц
select*
from orders
where date_added between now()- interval '1month' and now()
  and name_id = 2;
-- Найти заказ с максимальным количеством купленных цветов, вывести их
-- название и количество

SELECT f.title ,o.quantity as Количество
FROM orders o
         INNER JOIN flowers f on f.id = o.title_id
WHERE "quantity" = (SELECT MAX("quantity") from orders);

-- Вывести общую выручку (сумму золотых монет по всем заказам) за все
-- время

select sum(flowers.price * orders.quantity)
from flowers,
     orders
where flowers.id = orders.title_id;




