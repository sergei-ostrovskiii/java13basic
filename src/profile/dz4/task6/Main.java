package profile.dz4.task6;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    /*
    Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
     */
    public static void main(String[] args) {
        Set<Set<Integer>> x = Set.of(Set.of(0, 1, 2), Set.of(3, 4, 5), Set.of(6, 7, 8), Set.of(9, 10, 11),
                Set.of(12, 13, 14), Set.of(15, 16, 17), Set.of(18, 19, 20), Set.of(21, 22, 23),
                Set.of(0, 9, 21), Set.of(3, 10, 18), Set.of(6, 11, 15), Set.of(1, 4, 7),
                Set.of(16, 19, 22), Set.of(8, 12, 17), Set.of(5, 13, 20), Set.of(2, 14, 23));

        Set<Integer> st = x.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        System.out.println(st);


    }
}
