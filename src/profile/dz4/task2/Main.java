package profile.dz4.task2;

import java.util.List;
import java.util.stream.Stream;

public class Main {
    /*
    На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
     */
    public static void main(String[] args) {
        List<Integer> myPlaces = List.of(1, 2, 3, 4, 5);
        int sum = myPlaces.stream()
                .mapToInt(a -> a)
                .reduce(1, (a, b) -> a * b);

        System.out.println(sum);
    }
}
