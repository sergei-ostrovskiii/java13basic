package profile.dz4.task5;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    /*
    На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
     */
    public static void main(String[] args) {
        List<String> st = List.of("abc", "def", "qqq");
        System.out.println(st.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(",")));
    }
}
