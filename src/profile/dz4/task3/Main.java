package profile.dz4.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    /*
    . На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
     */
    public static void main(String[] args) {
        List<String> myPlaces = List.of("abc", "", "", "def", "qqq");
         long sc=myPlaces.stream()
                .filter(s -> s.length() > 0)
                .count();
        System.out.println(sc);
    }
}
