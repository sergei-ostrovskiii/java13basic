package profile.dz4.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    /*
    Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
     */
    public static void main(String[] args) {

       int sum=Stream
                .iterate(1, n -> ++n)
                .limit(100)
                .filter(o -> o % 2 == 0)
                .mapToInt(Integer::intValue)
                .sum();

        System.out.println(sum);

    }
}
