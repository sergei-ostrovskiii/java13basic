package profile.dz1.task6;

import profile.dz1.task4.Gender;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormValidator {
    //длина имени должна быть от 2 до 20 символов, первая буква заглавная.
    public static void checkName(String str) {
        try {
            if (str.length() > 1 && str.length() < 21)
                if (str.charAt(0) > 64 && str.charAt(0) < 91) {
                } else {
                    throw new Exception();
                }
        } catch (Exception e) {
            System.out.println("Невозможно создать имя");
        }
    }

    // дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты
    public void checkBirthdate(String str) {
        Pattern pt = Pattern.compile("[0-2][0-9]\\.[0-1][0-9]\\.[12][9012][0-9][0-9]");
        Matcher mt = pt.matcher(str);
        try {
            if (mt.matches()) {
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("Невозможно создать дату");
        }
    }

    // пол должен корректно матчится в enum Gender, хранящий Male и Female значения
    public void checkGender(String str) {

        try {
            if (Gender.Female.toString().equals(str)||Gender.Male.toString().equals(str)) {

            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("Невозможно создать пол");
        }
    }

    //рост должен быть положительным числом и корректно конвертироваться в double
    public void checkHeight(String str) {
        double x=Double.valueOf(str);
        try {
            if (x>0) {
                System.out.println(x);
            }else{
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("Невозможно создать рост");
        }
    }
}
