package profile.dz1.task4;

public class MyEvenNumber {
    private int n;

    public int getN() {
        return n;
    }

    public MyEvenNumber(int n) {
        try {
            if (n % 2 == 0) {
                this.n = n;
            }else{
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("Невозможно создать, введенно нечетное число");
        }
    }
}
