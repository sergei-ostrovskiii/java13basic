package profile.dz1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static final String FOLDER_DIRECTORY = "C:\\Users\\а\\Desktop\\проект номер 1\\src\\profile\\dz1\\task3\\file";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String st="C:\\Users\\а\\Desktop\\проект номер 1\\src\\profile\\dz1\\task3\\file\\input.txt";
        readFile(st);

    }

    public static void readFile(String filePath) {
        try {
            Scanner sc = new Scanner(new File(filePath));
            ArrayList<String> list = new ArrayList<>();
            while (sc.hasNextLine()) {
                list.add(sc.nextLine().toUpperCase());
            }
            Writer writer = new FileWriter(FOLDER_DIRECTORY + "/" + OUTPUT_FILE_NAME);
            for (int i = 0; i < list.size(); i++) {
                writer.write(list.get(i));
            }
            writer.close();
            sc.close();
        } catch (Exception e) {
            System.out.println("Проблема в считывании файла.");
        }
    }
}
