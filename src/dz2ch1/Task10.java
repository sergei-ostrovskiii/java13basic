package dz2ch1;
import java.util.Scanner;
public class Task10 {
    public static void main(String[] args) {
        game();
        }

    public static void game(){
        Scanner sc = new Scanner(System.in);
        int x = (int) (Math.random() * 1001);
        System.out.println("Угадайте число от 0 до 1000");
        while (true) {
            int p = sc.nextInt();
            if (x == p) {
                System.out.println("Победа!");
                break;
            }else if (p<=-1){
                break;
            } else if (x > p) {
                System.out.println("Это число меньше загаданного.");
            } else if (x < p && p > 0) {
                System.out.println("Это число больше загаданного.");
            }
        }
    }
}
