package firstweekpractice;

import java.util.Scanner;
// ctrl + alt + i выравнивание строк и пробелов

public class Task2 {
    public static void main(String [ ] args){
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int c = sc.nextInt();

        System.out.println("Стоимость 1ГБ трафика" + c * 1.0 / m);
        System.out.println("Стоимость 1ГБ трафика" + (double) c / m);
        System.out.println("Стоимость 1ГБ трафика" + c * 1D / m);
    }
}
