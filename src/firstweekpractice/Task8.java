package firstweekpractice;

import java.util.Scanner;

public class Task8 {
    /*
Дано целое число n.
Выведите следующее за ним четное число.
При решении этой задачи нельзя использовать условную инструкцию if и циклы.

5 -> 6
10 -> 12
 */

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int res = (n/2+1)*2;
        int res2 = n+2 - Math.abs(n) % 2;

        System.out.println(res);
        System.out.println(res2);
    }
}
