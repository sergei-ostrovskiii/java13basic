package firstweekpractice;

import java.util.Scanner;

public class Task4 {
    /*

Дана площадь круга, нужно найти диаметр окружности и длину окружности.
S  = PI * (D^2 / 4) - это через диаметр => d = sqrt(S * 4 / PI)
S = PI * r^2 - радиус
S = L^2 / (4 *PI) - площадь через длину
Отношение длины окружности к диаметру является постоянным числом.
π = L : d

Входные данные:
91
 */

    public static void main (String [] args){
        Scanner sc= new Scanner(System.in);
        int square = sc.nextInt();
        double d = Math.sqrt(square*4/ Math.PI);
        double l = Math.PI * d;
        System.out.println("Результат: d="+d+"; l = "+ l);
    }

    public static class Task6 {
    /*
            Дано двузначное число. Вывести сначала левую цифру (единицы), затем правую (десятки)
         */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int c = scanner.nextInt();

        System.out.println("Результат");
        System.out.println("Единицы: " + c % 10 + ";Десятки: " + c / 10);
    }

    }
}
