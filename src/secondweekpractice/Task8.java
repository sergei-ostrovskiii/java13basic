package secondweekpractice;

import java.util.Scanner;

public class Task8 {
    /*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a
 */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String a = sc.next();
        char c = a.charAt(0);

        if (c >= 'a' && c <= 'z') {
            System.out.println((char) (c + ('A' - 'a')));
        } else {
            System.out.println((char) (c - ('A' - 'a')));
        }


    }
}
