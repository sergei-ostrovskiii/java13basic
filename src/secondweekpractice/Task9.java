package secondweekpractice;

import java.util.Locale;
import java.util.Scanner;

public class Task9 {
    /*
   Дана строка и паттерн, заменить паттерн на паттерн, состоящий из заглавных символов
   Входные данные
   Hello
   o
   Выходные данные
   HellO

   Входные данные
   Hello world
   ld
   Выходные данные
   Hello worLD
    */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String pattern = sc.next();

        String upperPattern= pattern.toUpperCase();
        System.out.println(str.replace(pattern, upperPattern));


    }

}
